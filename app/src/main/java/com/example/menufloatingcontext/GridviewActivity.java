package com.example.menufloatingcontext;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class GridviewActivity extends AppCompatActivity {

    private GridView gridView;
    List<String> cursos = new ArrayList<>();
    ArrayAdapter<String> dataAdapter;

    @Override
    public boolean onContextItemSelected(@NonNull MenuItem item) {
//        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)
//                item.getMenuInfo();               //Recupera el item
        switch (item.getItemId()) {
            case 1:            //case R.id.item1:
                Toast.makeText(this, "Delete", Toast.LENGTH_SHORT).show();
                return true;
            case 2:
                Toast.makeText(this, "Share", Toast.LENGTH_SHORT).show();
                return true;
            case 3:
                Toast.makeText(this, "Web", Toast.LENGTH_SHORT).show();
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.setHeaderTitle("Choose an option");
        menu.add(0,1,1,"Delete");
        menu.add(0,2,2,"Share");
        menu.add(0,3,3,"Web");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gridview);

        //Hooks
        gridView = findViewById(R.id.listViewIm);

        //Fill array
        cursos.add("1JSIM");
        cursos.add("2JSIM");
        cursos.add("1HISX");
        cursos.add("2HISX");
        cursos.add("1HISM");
        cursos.add("2HISM");
        cursos.add("1HIAW");
        cursos.add("2HIAW");
        cursos.add("1WIAM");
        cursos.add("2WIAM");

        dataAdapter = new ArrayAdapter<String>(
                this,
                R.layout.support_simple_spinner_dropdown_item,
                cursos
        );
        gridView.setAdapter(dataAdapter);
        registerForContextMenu(gridView);


    }
}