package com.example.menufloatingcontext;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class ListViewFinal extends AppCompatActivity {

    private ListView listView;

    private ArrayList<Pokemon> pokemon = new ArrayList<>();

    public static final String EXTRA_IMAGE = "com.example.startup.EXTRA_IMAGE";
    public static final String EXTRA_IMAGE_BIG = "com.example.startup.EXTRA_IMAGE_BIG";
    public static final String EXTRA_TEXT_NAME = "com.example.startup.EXTRA_TEXT_NAME";
    public static final String EXTRA_TEXT_TYPE = "com.example.startup.EXTRA_TEXT_TYPE";
    public static final String EXTRA_TEXT_GEN = "com.example.startup.EXTRA_TEXT_GEN";
    public static final String EXTRA_TEXT_DESC = "com.example.startup.EXTRA_TEXT_DESC";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_view_final);

        //Hooks
        listView = findViewById(R.id.listViewFinalLV);

        initializeArray();

        CustomAdapter customAdapter = new CustomAdapter(this, pokemon);

        listView.setAdapter(customAdapter);
        registerForContextMenu(listView);

    }

    @Override
    public boolean onContextItemSelected(@NonNull MenuItem item) {
    AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();               //Recupera el item
        switch (item.getItemId()) {
            case 1:            //case R.id.item1:
                Toast.makeText(this, "Details", Toast.LENGTH_SHORT).show();

                displayInfo(info.position);

                return true;
            case 2:
                Toast.makeText(this, "Delete", Toast.LENGTH_SHORT).show();
                return true;
            case 3:
                Toast.makeText(this, "Share", Toast.LENGTH_SHORT).show();
                return true;
            case 4:
                Toast.makeText(this, "Web", Toast.LENGTH_SHORT).show();
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    private void displayInfo(int position) {

        Intent i = new Intent(getApplicationContext(), DetailActivity.class);

        i.putExtra(EXTRA_IMAGE, pokemon.get(position).getPokeImage());
        i.putExtra(EXTRA_IMAGE_BIG, pokemon.get(position).getPokeImage());
        i.putExtra(EXTRA_TEXT_NAME, pokemon.get(position).getPokeName());
        i.putExtra(EXTRA_TEXT_TYPE, pokemon.get(position).getPokeType());
        i.putExtra(EXTRA_TEXT_GEN, pokemon.get(position).getPokeGen());
        i.putExtra(EXTRA_TEXT_DESC, pokemon.get(position).getPokeDesc());

        startActivity(i);

    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.setHeaderTitle("Choose an option");
        menu.add(0,1,1,"Details");
        menu.add(0,2,2,"Delete");
        menu.add(0,3,3,"Share");
        menu.add(0,4,4,"Web");
    }

    private void initializeArray() {
        pokemon.add(new Pokemon("Bulbasaur", R.drawable.bulbasaur, "Planta/Veneno","Gen 1", "Bulbasaur es un Pokémon cuadrúpedo de color verde y manchas más oscuras de formas geométricas. Su cabeza representa cerca de un tercio de su cuerpo. En su frente se ubican tres manchas que pueden cambiar dependiendo del ejemplar. Tiene orejas pequeñas y puntiagudas. Sus ojos son grandes y de color rojo. Las patas son cortas con tres garras cada una. Este Pokémon tiene plantado un bulbo en el lomo desde que nace. Esta semilla crece y se desarrolla a lo largo del ciclo de vida de Bulbasaur a medida que suceden sus evoluciones. El bulbo absorbe y almacena la energía solar que Bulbasaur necesita para crecer. Dicen que cuanta más luz consuma la semilla, más olor producirá cuando se abra. Por otro lado, gracias a los nutrientes que el bulbo almacena, puede pasar varios días sin comer."));
        pokemon.add(new Pokemon("Chimchar", R.drawable.chimchar, "Fuego","Gen 4", "Chimchar está basado en un bebé babuino lo demuestran sus grandes orejas y su pequeño hocico, además que Chimchar tiene un copete, a pesar de que suele salir a una edad mayor en los babuinos, cuando les crece el cráneo.\nSu pelaje es de un color naranja pálido. Posee una llama en su cola que es producida quemándose el gas en su estómago; ésta no se apaga en la lluvia, aunque sí cuando va a dormir. Su voz recuerda a una cría de mono araña. Por ser un chimpancé, tiene gran agilidad, sobre todo en los árboles, esto se comprueba en el primer episodio de Diamante y Perla, y además porque el Chimchar de Ash en el sexto episodio (en manos de Paul/Polo en esas instancias) esquivaba muy veloz y ágilmente los ataques de Turtwig. Chimchar es muy cariñoso con su entrenador."));
        pokemon.add(new Pokemon("Darumaka", R.drawable.darumaka, "Fuego","Gen 5", "Su cuerpo es pequeño y redondo, con tonos rojos y amarillos. Está basado en un muñeco Daruma, un tipo de juguete japonés que representa a un personaje importante en Japón, al igual que Darmanitan.\nCuando un Darumaka se duerme, esconde sus brazos y sus patas, por lo que se dice que cuando está dormido no habrá nada que lo despierte; pero cuando está despierto es un Pokémon muy activo."));
        pokemon.add(new Pokemon("Deoxys", R.drawable.deoxys, "Psíquico","Gen 3", "Deoxys es una mutación de un virus espacial. La esfera en el cuerpo de este es en realidad su cerebro, de donde lanza descargas eléctricas. En la película, el cerebro de un Deoxys es verde o morado, pero en los videojuegos no hay ninguna diferencia en él. Al parecer, puede regenerarse completamente si su cerebro sobrevive, tal y como lo hace en el anime. En la película se ve que los Deoxys se comunican entre sí creando auroras del color de la esfera que tienen en el pecho.\nEs un Pokémon muy agresivo pero también bastante inteligente. Tiene como particularidad el poseer cuatro formas distintas (ataque, defensa, velocidad y normal) que cambian su aspecto, características y ataques que aprende por nivel, los cuales también varían según su forma. Curiosamente, sus brazos en su forma original se asemejan a la estructura del ADN."));
        pokemon.add(new Pokemon("Dragapult", R.drawable.dragapult, "Dragón/Fantasma","Gen 8", "Dragapult es un Pokémon con aparencia de un dragón con una gran cabeza triangular.\nTiene una cola larga de color azul claro que se vuelve transparente al final de la cola. Dragapult también tiene cuatro cuernos huecos en su cabeza triangular donde lanza a Dreepy desde los cuernos. Tiene dos brazos y pies pequeños, cada uno con garras afiladas, con las garras de las manos de color rosa brillante. Su cuerpo es negro y tiene un vientre amarillo con marcas rojas en forma de flecha.\nDragapult se preocupa por grupos de Dreepy. Los Dreepy viven con Dragapult pero se utilizan como misiles en la batalla. El movimiento es dracoflechas. Se desconoce cuando son disparados hacia al rival estos mueren o regresan junto a Dragapult para recargar.\nPuede evitar que bajen sus características a causa de movimientos o habilidades de otros Pokémon.\nAtaca sorteando la barrera o el sustituto del rival."));
        pokemon.add(new Pokemon("Gardevoir", R.drawable.gardevoir, "Psíquico/Hada","Gen 3", "Gardevoir es un Pokémon de apariencia humana; es uno de los Pokémon más caracterizados por su belleza, elegancia y poder durante el combate. El cuerpo de Gardevoir es largo y posee unas largas y finas piernas blancas que están recubiertas por un vestido también blanco pero verde por dentro. Gardevoir posee una cabeza grande cubierta por una especie de peinado verde, una cara blanca y pálida y unos ojos grandes y rojos.\nLos brazos de Gardevoir son delgados, verdes y sin rasgos distintivos. Además, posee unas curiosas consecuencias rojas sobre su torso delantero y su espalda que tienen la forma de una tarjeta de San Valentín.\nGardevoir tiene un sentido que ningún otro Pokémon posee, consistente en captar los sentimientos de su entrenador para poder ayudarle en caso de peligro, independientemente de si la vida de dicho Pokémon está en juego o no. Este Pokémon no sólo protege a su entrenador, también a sus descendientes. Además, el estado de ánimo de Gardevoir está estrechamente relacionado con el de su entrenador, es decir, que si su entrenador está preocupado o triste, Gardevoir también lo estará. Por el contrario, si su entrenador está contento o eufórico, Gardevoir se sentirá igual de bien."));
        pokemon.add(new Pokemon("Grimmsnarl", R.drawable.grimmsnarl, "Siniestro/Hada","Gen 8", "Su cuerpo, verde y muy delgado, está recubierto por un grueso y fuerte pelo, sobre todo alrededor de sus brazos, para tener más fuerza al golpear. Los pies no los cubre. Por detrás, el pelo le cae como una capa.\nEs un Pokémon muy agresivo y en rivalidad con los Machamp."));
        pokemon.add(new Pokemon("Hitmonchan", R.drawable.hitmonchan, "Lucha","Gen 1", "A diferencia de Hitmonlee, Hitmonchan es un maestro de los puños con los cuales consigue noquear a cualquier oponente. Su destreza con los golpes ha logrado que pueda aprender puñetazos de todas clases, incluyendo los elementales, entre otros.\nSe dice que Hitmonchan posee el espíritu de un boxeador que ha estado trabajando en el campeonato mundial. Tiene un espíritu indomable y nunca se da por vencido frente a sus adversarios. Por eso, se suele ver a Hitmonchan en concursos de boxeo profesional. Siempre usa un brazo para defenderse y otro para atacar, con lo cual nunca baja su defensa. Sus golpes se asemejan a una taladradora y pueden atravesar un muro de hormigón con gran facilidad. Pese a todo, Hitmonchan necesita un descanso tras 3 minutos de combate para recargar sus fuerzas."));
        pokemon.add(new Pokemon("Jolteon", R.drawable.jolteon, "Eléctrico","Gen 1", "Jolteon está inspirado en un zorro. Su pelaje le permite captar y almacenar gran cantidad de electricidad estática del ambiente, con lo que se eriza y endurece formando púas, que puede usar para concentrar y dirigir sus ataques eléctricos contra el rival. Sus pelos sólo se convierten en pinchos, cuando se siente amenazado, asustado o furioso. Sus células generan un nivel bajo de electricidad, que amplificada por la electricidad estática de su piel le permite lanzar rayos. Su cuello es de color blanco, el resto del cuerpo es de un amarillo intenso, sus ojos son negros. Es la única evolución de Eevee que carece de cola, la cual se confunde con su pelo. Se suele ver en las montañas o en ciudades, y también cerca de centrales eléctricas.\nTiene un carácter muy variable: en un momento puede estar contento y al rato enfadado, por lo que es un Pokémon difícil de llevar. Es un Pokémon orgulloso y arrogante a causa de su forma física, ya que tiene una gran agilidad y velocidad, haciéndolo uno de los Pokémon más rápidos que existen. Sus ataques son rápidos y constantes, por lo que es muy difícil escapar de su furia sin salir chamuscado."));
        pokemon.add(new Pokemon("Larvitar", R.drawable.larvitar, "Roca/Tierra","Gen 2", "Su alimento es básicamente todo lo que le rodea y que carezca de vida o movimiento. Larvitar nace de un huevo enterrado por Tyranitar. Antes se creía que estos Pokémon eran abandonados, pero se sabe que el lugar donde quedan enterrados siempre está custodiado por su madre a una distancia moderada y lo defenderá ferozmente. Se dice que sólo los Larvitar más fuertes suelen desenterrarse. Si por cosas del destino, este Pokémon llega a perderse de la supervisión de su madre y se encuentra en peligro, es capaz de generar un chirrido tan fuerte que hará aturdir a sus enemigos de inmediato.\nDebe alimentarse de lo que encuentre a su paso: basura, tierra, rocas, minerales, etc. Hasta que no sale de las profundidades de la tierra, no conoce a sus padres. Una vez que Larvitar está totalmente listo para evolucionar, devora una gigantesca montaña para luego echarse a descansar para que su piel escamosa lo cubra y se transforme en un Pupitar."));
        pokemon.add(new Pokemon("Noibat", R.drawable.noibat, "Volador/Dragón","Gen 6", "Noibat emite unas ondas ultrasónicas por las orejas que le permiten comunicarse con miembros de su misma especie o inmovilizar a su presa. También puede usar sus ondas para comprobar si la fruta que va a consumir ya está madura. Habita en cuevas donde reina la más absoluta oscuridad y al evolucionar desarrolla sus ondas para ser capaz de volar en las noches más oscuras.\nLos Noibat viven en grupos numerosos dentro de las cuevas. Si uno es atacado, este llama a los demás de su bandada para que acudan a su ayuda atacando entre todos y así poder huir."));
        pokemon.add(new Pokemon("Squirtle", R.drawable.squirtle, "Agua","Gen 1", "Squirtle es una de las especies más difíciles de encontrar. Habita tanto aguas dulces como marinas, preferiblemente zonas bastante profundas. Son pequeñas tortugas color celeste con caparazones color café; o rojas en algunos casos, con una cola enrollada que los distingue. Poco después de nacer, sus caparazones se endurecen y se hacen más resistentes a los ataques; muchos objetos rebotarán en ella.\nLa forma redonda de su caparazón y las figuras en su superficie hacen que Squirtle tenga una muy buena forma hidrodinámica, lo que le da mayor velocidad al nadar. Cuando se siente atacado, Squirtle esconde completamente su cuerpo en el interior de su caparazón, lo que hace que resulte imposible atacarle, además cuando esta dentro de su caparazón puede atacar escupiendo agua por todos los agujeros del caparazón. Es capaz de escupir agua por su boca con gran fuerza, ya sea para atacar o intimidar."));
        pokemon.add(new Pokemon("Torchic", R.drawable.torchic, "Fuego","Gen 3", "Este Pokémon está basado en un pequeño pollito. Torchic está cubierto por una suave capa de plumas con tonos anaranjados, sus alas son inútiles para volar debido a su pequeño tamaño. Antes que sus patas se desarrollen completamente, este Pokémon se mantiene cerca de su entrenador, siguiéndole a todos lados mientras aprende a caminar.\nTiene un saco interno en su estómago en el que hay fuego ardiendo todo el tiempo, lo que le permite arrojar bolas de fuego en batallas a una temperatura cercana a los 1000 °C, por ello Torchic está caliente al tacto. En las noches cuando va a dormir coloca su cabeza entre el plumaje de su espalda."));
        pokemon.add(new Pokemon("Torracat", R.drawable.torracat, "Fuego","Gen 7", "Al igual que su preevolución, Torracat esta basado en un felino. El saco de fuego bajo su cuello se asemeja a un cascabel como el que suelen llevar los gatos domésticos, aunque en apariencia Torracat recuerda más a un gato salvaje. La mitad superior de su cuerpo es de color negro, mientras que la inferior es de color rojo anaranjado así como sus bigotes, los anillos de la cola y el mechón de pelo en su frente.\nEl objeto con forma de cascabel que lleva en el cuello es un saco de fuego. Las emociones que siente Torracat provocan un aumento de la temperatura del saco de fuego, cuando desprende llamas para atacar emite un agudo y claro tintineo, su melena constituye un excelente órgano sensorial con el que sondea el entorno, incluso en plena oscuridad, para detectar también la presencia de enemigos ocultos."));
        pokemon.add(new Pokemon("Victini", R.drawable.victini, "Psíquico/Fuego","Gen 5", "Tiene aspecto de conejo, probablemente debido al mito de que las patas de conejo dan suerte; además de su forma, su color es blanco, como dicho animal. Produce energía infinita dentro de su cuerpo. Cuando lo toca un aliado, Victini comparte su energía. Este Pokémon atrae la victoria, se dice que cualquier entrenador que posea uno podrá ganar cualquier batalla. Curiosamente se encuentra en la Isla Jardín Libertad, encerrado hace 200 años por un guardián que salvaguarda la Isla Jardín Libertad con el fin de que el Equipo Plasma u otras organizaciones criminales no usaran su inmenso poder para hacer el mal. No se sabe dónde vive Victini originalmente, o puede ser un Pokémon migratorio que viaja para dar victoria a quien lo vea. Según la profesora Encina, Victini ocupa el n°000 en la Pokédex porque se dice que contar con él atrae la victoria."));
        pokemon.add(new Pokemon("Xurkitree", R.drawable.xurkitree, "Eléctrico","Gen 7", "Según algunos testigos, emana de su cuerpo descargas eléctricas de inmensa potencia. Invadió una central energética, lo que da a pensar que se alimenta de electricidad. Su característica más notable es el órgano que le permite generar energía. De hecho, su propia constitución recuerda a un tendido eléctrico. Gracias a ello, es capaz de controlar la energía con un alto grado de eficacia. Puede llegar a liberar una descarga eléctrica de un millón de voltios. Cuando no dispone de suficiente energía, clava ambas extremidades y la cola en el suelo para adoptar un aspecto parecido a un árbol. Mientras mantiene esta postura, puede absorber la energía del terreno."));

    }

    private class CustomAdapter extends BaseAdapter {

        private Context context;
        private ArrayList<Pokemon> pokemon;

        public CustomAdapter(Context context, ArrayList<Pokemon> pokemon) {
            this.context = context;
            this.pokemon = pokemon;
        }

        @Override
        public int getCount() {
            return pokemon.size();
        }

        @Override
        public Object getItem(int position) {
            return pokemon.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            View view = getLayoutInflater().inflate(R.layout.row_data3, null);

            ImageView imageRow = view.findViewById(R.id.imageRow3);
            TextView nameRow3 = view.findViewById(R.id.nameRow3);
            TextView typeRow3 = view.findViewById(R.id.typeRow3);
            TextView gen3Row3 = view.findViewById(R.id.genRow3);

            imageRow.setImageResource(pokemon.get(position).getPokeImage());
            nameRow3.setText(pokemon.get(position).getPokeName());
            typeRow3.setText(pokemon.get(position).getPokeType());
            gen3Row3.setText(pokemon.get(position).getPokeGen());

            return view;
        }
    }
}