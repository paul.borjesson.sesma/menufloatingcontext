package com.example.menufloatingcontext;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    Button listViewButton;
    Button gridViewButton;
    Button listIViewButton;
    Button gridIViewButton;
    Button listViewFButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Hooks
        listViewButton = findViewById(R.id.buttonLV);
        gridViewButton = findViewById(R.id.buttonGV);
        listIViewButton = findViewById(R.id.buttonLVI);
        gridIViewButton = findViewById(R.id.buttonGVI);
        listViewFButton = findViewById(R.id.buttonLVF);

        listViewButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ListviewActivity.class);
                startActivity(intent);
            }
        });

        gridViewButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, GridviewActivity.class);
                startActivity(intent);
            }
        });

        listIViewButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ListViewWithImages.class);
                startActivity(intent);
            }
        });

        gridIViewButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, GridViewWithImages.class);
                startActivity(intent);
            }
        });

        listViewFButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ListViewFinal.class);
                startActivity(intent);
            }
        });

    }
}