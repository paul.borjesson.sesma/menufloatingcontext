package com.example.menufloatingcontext;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class GridViewWithImages extends AppCompatActivity {

    private GridView gridView;

    private Integer[] imageIDs = {
            R.drawable.bulbasaur,
            R.drawable.chimchar,
            R.drawable.darumaka,
            R.drawable.deoxys,
            R.drawable.dragapult,
            R.drawable.gardevoir,
            R.drawable.grimmsnarl,
            R.drawable.hitmonchan,
            R.drawable.jolteon,
            R.drawable.larvitar,
            R.drawable.noibat,
            R.drawable.squirtle,
            R.drawable.torchic,
            R.drawable.torracat,
            R.drawable.victini,
            R.drawable.xurkitree
    };

    private String[] imageText = {"Bulbasaur", "Chimchar", "Darumaka", "Deoxys", "Dragapult", "Gardevoir", "Grimmsnarl", "Hitmonchan", "Jolteon", "Jolteon",
            "Larvitar", "Noibat", "Squirtle", "Torchic", "Torracat", "Victini", "Xurkitree"};

    CustomAdapter customAdapter = new CustomAdapter();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grid_view_with_images);

        //Hooks
        gridView = findViewById(R.id.listViewIm);

        gridView.setAdapter(customAdapter);
        registerForContextMenu(gridView);

    }

    @Override
    public boolean onContextItemSelected(@NonNull MenuItem item) {
//        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)
//                item.getMenuInfo();               //Recupera el item
        switch (item.getItemId()) {
            case 1:            //case R.id.item1:
                Toast.makeText(this, "Delete", Toast.LENGTH_SHORT).show();
                return true;
            case 2:
                Toast.makeText(this, "Share", Toast.LENGTH_SHORT).show();
                return true;
            case 3:
                Toast.makeText(this, "Web", Toast.LENGTH_SHORT).show();
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.setHeaderTitle("Choose an option");
        menu.add(0,1,1,"Delete");
        menu.add(0,2,2,"Share");
        menu.add(0,3,3,"Web");
    }

    private class CustomAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return imageIDs.length;
        }

        @Override
        public Object getItem(int position) {
            return imageIDs[position];
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = getLayoutInflater().inflate(R.layout.row_data, null);
            ImageView imageRow = view.findViewById(R.id.imageRow3);
            TextView textRow = view.findViewById(R.id.nameRow3);

            imageRow.setImageResource(imageIDs[position]);
            textRow.setText(imageText[position]);

            return view;
        }
    }
}