package com.example.menufloatingcontext;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class DetailActivity extends AppCompatActivity {

    private ImageView image;
    private ImageView imageBig;
    private TextView name;
    private TextView type;
    private TextView gen;
    private TextView desc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        //Hooks
        image = findViewById(R.id.imageDetAct);
        imageBig = findViewById(R.id.imageBigDetAct);
        name = findViewById(R.id.nameDetAct);
        type = findViewById(R.id.typeDetAct);
        gen = findViewById(R.id.genDetAct);
        desc = findViewById(R.id.descDetAct);

        Intent intent = getIntent();

        image.setImageResource(intent.getIntExtra(ListViewFinal.EXTRA_IMAGE, 1));
        imageBig.setImageResource(intent.getIntExtra(ListViewFinal.EXTRA_IMAGE_BIG, 1));
        name.setText(intent.getStringExtra(ListViewFinal.EXTRA_TEXT_NAME));
        type.setText(intent.getStringExtra(ListViewFinal.EXTRA_TEXT_TYPE));
        gen.setText(intent.getStringExtra(ListViewFinal.EXTRA_TEXT_GEN));
        desc.setText(intent.getStringExtra(ListViewFinal.EXTRA_TEXT_DESC));

    }
}