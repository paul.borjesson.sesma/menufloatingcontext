package com.example.menufloatingcontext;

public class Pokemon {

    private String pokeName;
    private int pokeImage;
    private String pokeType;
    private String pokeGen;
    private String pokeDesc;

    public Pokemon(String pokeName, int pokeImage, String pokeType, String pokeGen, String pokeDesc) {
        this.pokeName = pokeName;
        this.pokeImage = pokeImage;
        this.pokeType = pokeType;
        this.pokeGen = pokeGen;
        this.pokeDesc = pokeDesc;
    }

    public String getPokeName() {
        return pokeName;
    }

    public int getPokeImage() {
        return pokeImage;
    }

    public String getPokeType() {
        return pokeType;
    }

    public String getPokeGen() {
        return pokeGen;
    }

    public String getPokeDesc() {
        return pokeDesc;
    }
}
